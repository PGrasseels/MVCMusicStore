﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCMusicStore.Models;

namespace MVCMusicStore.Controllers
{
    public class StoreController : Controller
    {
        MusicStoreEntities storeDB = new MusicStoreEntities();

        //
        // GET: /Store/

        public ActionResult Index()
        {
            var Genre = storeDB.Genres.ToList();
            return View(Genre);
        }

        //
        // GET: /Store/Browse
        public ActionResult Browse(string Genre)
        {
            var Ge = storeDB.Genres.Include("Albums").Single(g => g.Name == Genre);
            return View(Ge);
        }

        //
        // GET: /Store/Detail
        public ActionResult Detail(int ID)
        {
            Album Alb = storeDB.Albums.Find(ID);
            return View(Alb);
        }

    }
}
